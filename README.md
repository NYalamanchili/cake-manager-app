# README #

### What is this repository for? ###

To store, retrieve and update Cakes.

### How do I get set up? ###

This is a SpringBoot application runs on default tomcat port 8080. It's configured with Swagger which can be accesses at http:localhost:8080/store/cakes/v1/api-docs and Swagger-UI at http://localhost:8080/swagger-ui.html where you can see the list of apis.
You can either runs it as a normal SpringBoot application or you can run it as a docker container. In order for it to run as docker container execute the mvn clean install docker:build which will build docker image
'cake-manager-app-container'. You can run as docker run -p 8080:8080 followed by ImageId.
The application is configured with a CI and CD using Bitbucket pipeline so for any commits that pushed to the master it runs mvn clean install docker:build and deploys finally the app to Heroku cloud.
It's using in-memory MongoDB with Spring-Data.
The url for Heroku would have been provided but the app is crashing there because of the limit to the free account in terms of memory. 

Basic API implementation and unit test cases are added because of limited time.
Adding Image part is left out as it would have to be a separate API where the user can store it as a separate resource  and link it with Cake resource.