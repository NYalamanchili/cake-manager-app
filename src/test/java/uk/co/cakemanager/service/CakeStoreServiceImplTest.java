package uk.co.cakemanager.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.math.BigDecimal;
import java.util.Optional;

import org.bson.types.ObjectId;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import uk.co.cakemanager.model.Cake;
import uk.co.cakemanager.repository.CakeStoreRepository;
import uk.co.cakemanager.service.exception.CakeStoreServiceException;


@RunWith(MockitoJUnitRunner.class)
public class CakeStoreServiceImplTest {

	@Mock
	private CakeStoreRepository cakeStoreRepository;

	@InjectMocks
	private CakeStoreServiceImpl cakeStoreServiceImpl;
	
	@Test
	public void testStoreCake() {
		Cake cake = createCake(null, "desc", "title", "type");
		Mockito.when(cakeStoreRepository.insert(Mockito.any(Cake.class))).thenReturn(createCake(new ObjectId("5b5104d3c9ad3744284853a7"), "desc", "title", "type"));
		String id = cakeStoreServiceImpl.storeCake(cake);
		ArgumentCaptor<Cake> argument = ArgumentCaptor.forClass(Cake.class);
		Mockito.verify(cakeStoreRepository).insert(argument.capture());
		assertNull(argument.getValue().get_id());
		assertEquals("5b5104d3c9ad3744284853a7", id);
	}
	
	@Test
	public void testUpdate() {
		Mockito.when(cakeStoreRepository.existsById("5b5104d3c9ad3744284853a7")).thenReturn(true);
		Cake cake = createCake(new ObjectId("5b5104d3c9ad3744284853a7"), "desc1", "title1", "type1");		
		
		cakeStoreServiceImpl.updateCake(cake);
		
		Mockito.verify(cakeStoreRepository).existsById("5b5104d3c9ad3744284853a7");
		Mockito.verify(cakeStoreRepository, Mockito.times(1)).save(Mockito.any(Cake.class));
	}
	
	@Test(expected=CakeStoreServiceException.class)
	public void testUpdateCakeNotFound() {
		Mockito.when(cakeStoreRepository.existsById("5b5104d3c9ad3744284853a7")).thenReturn(false);
		Cake cake = createCake(new ObjectId("5b5104d3c9ad3744284853a7"), "desc1", "title1", "type1");		
		cakeStoreServiceImpl.updateCake(cake);
		
		Mockito.verify(cakeStoreRepository).existsById("5b5104d3c9ad3744284853a7");
		Mockito.verify(cakeStoreRepository, Mockito.times(0)).save(Mockito.any(Cake.class));
	}
	
	@Test
	public void testGetCakes() {
		//TODO implement
	}
	
	
	private Cake createCake(ObjectId id, String description, String title, String type) {
		Cake cake = new Cake();
		cake.set_id(id);
		cake.setDescription(description);
		cake.setTitle(title);
		cake.setType(type);
		return cake;
	}
}
