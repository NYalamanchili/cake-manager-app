package uk.co.cakemanager.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.text.ParseException;

import javax.validation.Valid;

import org.bson.types.ObjectId;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.mongodb.MongoWriteException;
import com.mongodb.WriteError;

import uk.co.cakemanager.model.Cake;
import uk.co.cakemanager.requestparam.CakeStoreRequestParam;
import uk.co.cakemanager.service.CakeStoreService;

@RunWith(MockitoJUnitRunner.class)
public class CakeStoreControllerTest {

	@Mock
	private CakeStoreService cakeStoreService;

	@InjectMocks
	private CakeStoreController cakeStoreController;

	@Test
	public void testStoreCake() {
		Mockito.when(cakeStoreService.storeCake(Mockito.any(Cake.class))).thenReturn("objectId");
		
		CakeStoreRequestParam csRequestParam = new CakeStoreRequestParam("mockTitle", "description", "mockType");
		
		ResponseEntity<String> response = cakeStoreController.storeCake(csRequestParam);

		ArgumentCaptor<Cake> argument = ArgumentCaptor.forClass(Cake.class);

		Mockito.verify(cakeStoreService).storeCake(argument.capture());

		assertEquals("mockTitle", argument.getValue().getTitle());
		assertEquals("description", argument.getValue().getDescription());
		assertEquals("mockType", argument.getValue().getType());
		assertEquals("objectId", response.getBody());
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
	}

	
	@Test
	public void testUpdateCake() {
		Mockito.doNothing().when(cakeStoreService).updateCake(Mockito.any(Cake.class));
		
		CakeStoreRequestParam csRequestParam = new CakeStoreRequestParam("mockTitle", "description", "mockType");
		
		ResponseEntity<String> response = cakeStoreController.updateCake("5c7d996a418d3b185fbc0ec6", csRequestParam);

		ArgumentCaptor<Cake> argument = ArgumentCaptor.forClass(Cake.class);

		Mockito.verify(cakeStoreService).updateCake(argument.capture());
		
		assertEquals("5c7d996a418d3b185fbc0ec6", argument.getValue().get_id());
		assertEquals("mockTitle", argument.getValue().getTitle());
		assertEquals("description", argument.getValue().getDescription());
		assertEquals("mockType", argument.getValue().getType());
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
	
	@Test(expected = DuplicateKeyException.class)
	public void testCakeAlreadyExists() {
		
		Mockito.when(cakeStoreService.storeCake(Mockito.any(Cake.class))).thenThrow(new DuplicateKeyException("Cake already exists"));

		CakeStoreRequestParam csRequestParam = new CakeStoreRequestParam("mockTitle", "description", "mockType");
		
		cakeStoreController.storeCake(csRequestParam);

		ArgumentCaptor<Cake> argument = ArgumentCaptor.forClass(Cake.class);

		Mockito.verify(cakeStoreService).storeCake(argument.capture());
	}
}
