package uk.co.cakemanager.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import uk.co.cakemanager.model.Cake;

public interface CakeStoreRepository extends MongoRepository<Cake, String> {
	Cake save(Cake cake);
}
