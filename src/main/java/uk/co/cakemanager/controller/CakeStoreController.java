package uk.co.cakemanager.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import uk.co.cakemanager.controller.parammapper.RequestParamMapper;
import uk.co.cakemanager.model.Cake;
import uk.co.cakemanager.requestparam.CakeStoreRequestParam;
import uk.co.cakemanager.service.CakeStoreService;

@RestController
@Validated
public class CakeStoreController {

	private static Log LOGGER = LogFactory.getLog(CakeStoreController.class);

	@Autowired
	private CakeStoreService cakeStoreService;

	@RequestMapping(value = "/store/cakes/cake", method = RequestMethod.POST)
	public ResponseEntity<String> storeCake(@Valid @RequestBody CakeStoreRequestParam csRequestParam) {
		LOGGER.info("add new cake");
		String id = cakeStoreService.storeCake(RequestParamMapper.TO_CAKE.map(csRequestParam, null));
		return new ResponseEntity<String>(id, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/store/cakes/{id}", method = RequestMethod.PUT)
	public ResponseEntity<String> updateCake(@PathVariable String id,
			@Valid @RequestBody CakeStoreRequestParam csRequestParam) {
		LOGGER.info("updating cake");
		cakeStoreService.updateCake(RequestParamMapper.TO_CAKE.map(csRequestParam, id));
		return new ResponseEntity<String>("", HttpStatus.OK);
	}

	@RequestMapping(value = "/store/cakes", method = RequestMethod.GET)
	public ResponseEntity<List<Cake>> cakes() {
		LOGGER.info("updating cake");
		List<Cake> cakes = cakeStoreService.getCakes();
		return new ResponseEntity<List<Cake>>(cakes, HttpStatus.OK);
	}

}
