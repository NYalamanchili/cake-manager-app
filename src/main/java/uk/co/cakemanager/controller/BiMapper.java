package uk.co.cakemanager.controller;

public interface BiMapper<T1, T2, R> {

	R map(T1 requestParam, T2 id);
}
