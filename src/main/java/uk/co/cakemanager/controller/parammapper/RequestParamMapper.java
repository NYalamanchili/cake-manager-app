package uk.co.cakemanager.controller.parammapper;

import org.bson.types.ObjectId;

import uk.co.cakemanager.controller.BiMapper;
import uk.co.cakemanager.model.Cake;
import uk.co.cakemanager.requestparam.CakeStoreRequestParam;

public class RequestParamMapper {

	public static final BiMapper<CakeStoreRequestParam, String, Cake> TO_CAKE = (csrp, id) -> {
		Cake cake = new Cake();
		cake.setDescription(csrp.getDescription());
		cake.setTitle(csrp.getTitle());
		cake.setType(csrp.getCakeType());
		ObjectId resourceId = id != null ? new ObjectId(id) : null;
		cake.set_id(resourceId);
		return cake;
	};
}
