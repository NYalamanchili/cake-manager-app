package uk.co.cakemanager.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.mongodb.MongoWriteException;

import uk.co.cakemanager.requestparam.ErrorInfo;
import uk.co.cakemanager.service.exception.CakeStoreServiceException;

@ControllerAdvice
class GlobalExceptionHandler {

	private static final Log LOGGER = LogFactory.getLog(GlobalExceptionHandler.class);

	@ExceptionHandler(value = { DuplicateKeyException.class, MongoWriteException.class })
	ResponseEntity<ErrorInfo> handleConflict(HttpServletRequest request, HttpServletResponse response, Exception ex) {
		LOGGER.error("Resource already exists");
		ErrorInfo errorInfo = new ErrorInfo(request.getRequestURL().toString(), ex,
				"Duplicate resource", HttpStatus.CONFLICT);
		return new ResponseEntity<ErrorInfo>(errorInfo, HttpStatus.CONFLICT);
	}
	
	@ExceptionHandler(value = {CakeStoreServiceException.class })
	ResponseEntity<ErrorInfo> handleServiceException(HttpServletRequest request, HttpServletResponse response, CakeStoreServiceException csse) {
		LOGGER.error("Resource not found");
		ErrorInfo errorInfo = new ErrorInfo(request.getRequestURL().toString(), csse,
				"Cake not found", HttpStatus.NOT_FOUND);
		return new ResponseEntity<ErrorInfo>(errorInfo, HttpStatus.NOT_FOUND);
	}

}
