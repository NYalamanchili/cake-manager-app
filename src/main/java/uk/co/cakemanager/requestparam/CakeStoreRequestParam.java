package uk.co.cakemanager.requestparam;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CakeStoreRequestParam {
	@JsonProperty("cake_title")
	@NotNull
	@Pattern(regexp = "^[A-Za-z0-9]+$")
	private String title;
	@JsonProperty("cake_description")
	@NotNull
	@Pattern(regexp = "^[A-Za-z.]+$")
	private String description;
	@JsonProperty("cake_type")
	@NotNull
	@Pattern(regexp = "^[A-Za-z0-9]+$")
	private String cakeType;

	public CakeStoreRequestParam(String title, String description, String cakeType){
		this.title = title;
		this.description = description;
		this.cakeType = cakeType;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getCakeType() {
		return cakeType;
	}
}
