package uk.co.cakemanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.test.context.ActiveProfiles;

@ComponentScan({"uk.co.cakemanager.*"})
@EnableMongoRepositories
@SpringBootApplication
@ActiveProfiles({ "local" })
@EnableAutoConfiguration
public class CakeManagerApp {
	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(CakeManagerApp.class);
		app.setLogStartupInfo(true);
		app.run(args);
	}
}
