package uk.co.cakemanager.service;

import java.util.List;

import uk.co.cakemanager.model.Cake;

public interface CakeStoreService {
	String storeCake(Cake cake);

	void updateCake(Cake cake);

	List<Cake> getCakes();
}
