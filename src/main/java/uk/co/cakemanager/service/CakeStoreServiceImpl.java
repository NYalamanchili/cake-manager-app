package uk.co.cakemanager.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import uk.co.cakemanager.model.Cake;
import uk.co.cakemanager.repository.CakeStoreRepository;
import uk.co.cakemanager.service.exception.CakeStoreServiceException;

@Component
public class CakeStoreServiceImpl implements CakeStoreService {

	@Autowired
	private CakeStoreRepository cakeStoreRepository;

	@Override
	public String storeCake(Cake cake) {
		return cake != null ? cakeStoreRepository.insert(cake).get_id() : null;
	}

	@Override
	public void updateCake(Cake cake) {
		if (doesCakeExist(cake.get_id())) {
			cakeStoreRepository.save(cake);
		} else {
			throw new CakeStoreServiceException("Cake not found "+cake.get_id());
		}
	}

	private boolean doesCakeExist(String id) {
		return StringUtils.isNotEmpty(id) ? cakeStoreRepository.existsById(id) : false;
	}

	@Override
	public List<Cake> getCakes() {
		return cakeStoreRepository.findAll();
	}
}
