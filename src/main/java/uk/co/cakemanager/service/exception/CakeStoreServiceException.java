package uk.co.cakemanager.service.exception;

public class CakeStoreServiceException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public CakeStoreServiceException(String message) {
		super(message);
	}

}

